'use strict';

const path = require('path'),
      nconf = require('nconf'),
      findRemoveSync = require('find-remove');
      
nconf.argv().env('__').defaults({'NODE_ENV': 'development'});

const NODE_ENV = nconf.get('NODE_ENV'),
      isDev = NODE_ENV === 'development';

nconf
  .defaults({'conf': path.join(__dirname, `${NODE_ENV}.config.json`)})
  .file(nconf.get('conf'));

setInterval(() => {
    const removeStatics = findRemoveSync(nconf.get('staticDir'), {extensions: '.zip', age:{seconds: nconf.get('maxFileAge')}, test: isDev}),
          removeTemp = findRemoveSync(nconf.get('uploadDir'), {files: '*.*', age:{seconds: nconf.get('maxFileAge')}, test: isDev});
    
    if (isDev) {
      console.log('Archivos estaticos eliminados:\n' + JSON.stringify(removeStatics));
      console.log('Archivos temporales eliminados:\n' + JSON.stringify(removeTemp));
    }
    
  }, nconf.get('interval')
);