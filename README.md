**REQUERIMIENTOS**

  * Ubuntu > 18.x
  * Nodejs > 8.x
  * npm > 6.x (creo que se instala por omisión con nodejs)
  * [pm2](https://pm2.io) > 3.3.x

**INSTALACION**

0. Se asume que los siguientes directorios existen y el usuario que ejecuta este script tiene permisos para escribir en ellos:

    `/datos/geoapi/upload/`
    
    `/datos/geoapi/temp/`

1. Asumiendo que este directorio existe y se tienen permisos de escritura:

    `cd /opt/webserver`

2. Clonar el repositorio:

    `git clone https://artorresv@bitbucket.org/artorresv/geoapi.remove.git`
    
3. El procedimiento anterior crea un directorio con el código fuente:

    `cd geoapi.remove`

4. Instalar las dependencias de nodejs:

    `NODE_ENV=production npm install`

5. Después de instaladas, copiar los siguientes archivos (revisar el contenido para ver qué puerto usa, etc.) según la instalación sea para desarrollo o para producción:

    Producción: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi.remove/production.config.json .`
    
    Desarrollo: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi.remove/development.config.json .`

**USO**

1. Incio normal de la aplicación:
    
    a. Con npm:
    
       `NODE_ENV=production npm run start`
       
    b. Con pm2 (ver ecosystem.config.json para más detalles):
    
       `pm2 start ecosystem.config.json --env production`
      
2. Incio para [depuración](https://nodejs.org/en/docs/guides/debugging-getting-started/) con npm:

    `NODE_ENV=production npm run start:debug`
    
3. Operaciones:
      
    El script busca periódicamente archivos con más de 72 horas de existencia de los directorios:
    
    * `/datos/geoapi/temp/`
    
    * `/datos/geoapi/upload/`
    
    En ambos directorios se guardan archivos estáticos de otros dos servicios, geoapi.downloads y geoapi
         
4. Comentarios:

    * Ninguno
     
**ADVERTENCIAS**

  * Ninguna

**PENDIENTES**

  * Ninguno